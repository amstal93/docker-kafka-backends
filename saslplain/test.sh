#!/usr/bin/env bash

SASL_DIR="sasl"
JAAS_CONFIG="kafka_server_jaas.conf"

SASL_JAAS_CONFIG="$SASL_DIR/$JAAS_CONFIG"

mkdir -p foobar

if [ ! -f $SASL_JAAS_CONFIG ]; then
  echo 'KafkaServer {
    org.apache.kafka.common.security.plain.PlainLoginModule required
    username="admin"
    password="admin-pass"
    user_admin="admin-pass"
    user_luke="luke-pass"
    user_vader="vader-pass";
  };' > $SASL_JAAS_CONFIG
else
  echo "Using existing $JAAS_CONFIG file"
fi