#!/usr/bin/env bash

# Create topics
kafka-topics --zookeeper 192.168.1.22:32181 --topic kpm-test-topic-1 --create --replication-factor 3 --partitions 9
kafka-topics --zookeeper 192.168.1.22:32181 --topic kpm-test-topic-2 --create --replication-factor 3 --partitions 9
kafka-topics --zookeeper 192.168.1.22:32181 --topic kpm-test-topic-3 --create --replication-factor 3 --partitions 9
kafka-topics --zookeeper 192.168.1.22:32181 --topic kpm-test-topic-4 --create --replication-factor 3 --partitions 9

# Luke permissions
kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer \
  --authorizer-properties zookeeper.connect=localhost:32181 \
  --add --allow-principal User:luke \
  --operation Read \
  --group luke-test-group

kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer \
  --authorizer-properties zookeeper.connect=localhost:32181 \
  --add --allow-principal User:luke \
  --operation Write \
  --operation Read \
  --operation Describe \
  --topic kpm-test-topic-1

kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer \
  --authorizer-properties zookeeper.connect=localhost:32181 \
  --add --allow-principal User:luke \
  --operation Write \
  --operation Read \
  --operation Describe \
  --topic kpm-test-topic-2

# Vader permissions
kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer \
  --authorizer-properties zookeeper.connect=localhost:32181 \
  --add --allow-principal User:vader \
  --operation Read \
  --group vader-test-group

kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer \
  --authorizer-properties zookeeper.connect=localhost:32181 \
  --add --allow-principal User:vader \
  --operation Read \
  --operation Describe \
  --topic kpm-test-topic-1

# group permission
kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer \
  --authorizer-properties zookeeper.connect=localhost:32181 \
  --add --allow-principal Group:vader-test-group \
  --operation Write \
  --operation Read \
  --operation Describe \
  --topic kpm-test-topic-4